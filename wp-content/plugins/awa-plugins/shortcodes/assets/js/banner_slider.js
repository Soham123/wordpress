;(function ($, window, document, undefined) {
    'use strict';

    $('.btn-scroll-down').on('click', function () {
        $('html, body').animate({
            scrollTop: $(window).height()
        }, 600);
        return false;
    });

    if ($('.banner-slider-wrap.horizontal, .banner-slider-wrap.horizontal_modern, .top-banner__scene, .banner-slider-wrap.vertical_custom_elements').length) {

        if ($(window).width() > 767) {
            $('.banner-slider-wrap.horizontal, .banner-slider-wrap.horizontal_modern, .top-banner__scene, .banner-slider-wrap.vertical_custom_elements').each(function () {
                var items = $(this).find('.images-wrap');

                items.each(function () {
                    var id = $(this).attr('id');
                    var scene = document.getElementById(id);
                    var parallaxInstance = new Parallax(scene, {
                        relativeInput: false,
                        clipRelativeInput: false,
                        calibrationThreshold: 100,
                        calibrationDelay: 500,
                        supportDelay: 500,
                        calibrateX: true,
                        calibrateY: false,
                        invertX: true,
                        invertY: true,
                        limitX: false,
                        limitY: false,
                        scalarX: 5.0,
                        scalarY: 5.0,
                        frictionX: 0.1,
                        frictionY: 0.1,
                        originX: 0.5,
                        originY: 0.5,
                        hoverOnly: true
                    });
                });
            });
        }
    }

    function bannerSliderHeight() {
        if ($('.banner-slider-wrap .container').length) {
            var contentH = $('.banner-slider-wrap .container').outerHeight();
            var winH = $(window).height();
            var bannerH = winH > (contentH + 200) ? winH : (contentH + 200);
            $('.banner-slider-wrap .slider-banner').css('height', bannerH);
        }
    }

	function supersizedSlider(){
		if($('.banner-slider-supersized').length){
			$('.banner-slider-supersized').each(function () {

				var $t = $(this);
				var autoplay = parseInt($t.attr('data-autoplay'), 10) === 0 ? '500000' : parseInt($t.attr('data-autoplay'), 10),
					speed = parseInt($t.attr('data-speed'), 10),
					images = $t.attr('data-images') ? $t.attr('data-images') : '';

				// $t.find('.content-wrap').css('transition-delay', speed);

				var slides = images.split(',');
				var imgArray = [];


				$.each( slides, function (i, val) {
					var  imgObj = {};
					imgObj["image"] = val;
					imgArray.push(imgObj);
				});

				$t.supersized({

					// Functionality
					slide_interval: autoplay,		// Length between transitions
					transition : 5, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
					transition_speed :	speed,		// Speed of transition

					// Components
					slide_links: false,	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
					slides: imgArray,
					thumb_links: 0

				});
			});
		}
	}

	function gridrotatorGallery() {
		if($('.gridrotate-gallery .gridrotate-banner').length){

			if($('.gridrotate-gallery .title').length) {

				$('.gridrotate-gallery .title').each(function () {
					var head = $(this);
					var typingWords = head.find('.typed').data('words'),
						wordsArray = typingWords.split(',');

					head.find('.typed').each(function () {
						$(this).typed({
							strings: wordsArray,
							// Optionally use an HTML element to grab strings from (must wrap each string in a <p>)
							stringsElement: null,
							// typing speed
							typeSpeed: 30,
							// time before typing starts
							startDelay: 1200,
							// backspacing speed
							backSpeed: 20,
							// time before backspacing
							backDelay: 500,
							// loop
							loop: true,
							// false = infinite
							loopCount: false,
							// show cursor
							showCursor: true,
							// character for cursor
							cursorChar: "",
							// attribute to type (null == text)
							attr: null,
							// either html or text
							contentType: 'html',
							// call when done callback function
						});
					})
				});
			}

			$('.gridrotate-gallery .gridrotate-banner').each(function () {
				$(this).gridrotator({
					rows : 4,
					// number of columns
					columns : 8,
					w1200 : { rows : 4, columns : 7 },
					w992 : { rows : 7, columns : 5 },
					w510 : { rows : 7, columns : 4 },
					w480 : { rows : 7, columns : 4 },
					w320 : { rows : 7, columns : 4 },
					step : 7,
					maxStep : 7
				});

			});
		}
	}


    $(window).on('load resize', function () {
        bannerSliderHeight();
	    supersizedSlider();
	    gridrotatorGallery();
    });


    window.addEventListener("orientationchange", function () {
        bannerSliderHeight();
    });

})(jQuery, window, document);